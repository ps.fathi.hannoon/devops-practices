from openjdk:latest

Run mkdir -p /u01/pdp \ 
    && chmod 777 -R /u01 

copy /target/*.jar /u01/pdp/project.jar
expose 8070 
CMD java -jar -Dserver.port=8070 /u01/pdp/project.jar 
